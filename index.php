<?php get_header(); ?>

		<?php get_template_part("includes/navbar", "menu"); ?>
  		
  		<?php get_template_part("includes/slider", "home"); ?>
  		
  		<div class="container">
	  		<div class="row dark-grey" id="home_header">
		  		<div class="col s12">
			  		<div class="row">
				  		<div class="col s12 m10 offset-m1 l10 offset-l1">
					  		<div class="space40"></div>
				  			<h1 class="conduit-black font36 block conifer-text"><?php the_field("home_titulo", "option"); ?></h1>
				  			<h2 class="conduit font26 block conifer-text"><?php the_field("home_texto", "option"); ?></h2>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
	  		<?php $programacion = get_field("home_programacion", "option"); ?>
	  		<div class="row conifer no-margin-row" id="home_programacion">
		  		<div class="col s12">
			  		<div class="space20"></div>
			  		<div class="row padding-1rem">
				  		<?php foreach ($programacion as $seccion) { ?>
				  		<div class="col s6 m4 l4">
					  		<span class="conduit-black font36 block violet-text uppercase"><?php echo $seccion['nombre']; ?></span>
					  		<div class="space20"></div>
					  		<?php foreach ($seccion['lista'] as $programa) { ?>
					  		<span class="conduit font20 block violet-text"><?php echo $programa['nombre']; ?></span>
					  		<?php } ?>
				  		</div>
				  		<?php } ?>
			  		</div>
		  		</div>
	  		</div>
	  		<?php $videos_destacados = get_field("home_videos_destacados", "option"); $contador_videos = 0; ?>
	  		<div class="row dark-grey no-margin-row" id="home_videos_destacados">
	  			<div class="col s12">
			  		<div class="space20"></div>
			  		<div class="row">
				  		<div class="col s12 m12 l10 offset-l1">
					  		<span class="conduit-black font36 block conifer-text uppercase centered">/Videos Destacados</span>
					  		<div class="space20"></div>
					  		<div class="row">
						  		<?php foreach ($videos_destacados as $video) { $contador_videos++; ?>
						  		<div class="col s12 m6 l6 home_video_destacado" rel="<?php echo $contador_videos; ?>">
							  		<center>
							  			<img alt="Portada Video" longdesc="<?php echo $video['portada']; ?>" src="<?php echo $video['portada']; ?>" class="responsive-img" />
							  			<input type="hidden" id="portada-vd-<?php echo $contador_videos; ?>" name="portada-vd-<?php echo $contador_videos; ?>" value="<?php echo $video['portada']; ?>" />
							  			<input type="hidden" id="portada-vd-hover-<?php echo $contador_videos; ?>" name="portada-vd-hover-<?php echo $contador_videos; ?>" value="<?php echo $video['hover']; ?>" />
							  		</center>
							  		<!-- Modal Structure -->
							  		<div id="video_destacado_<?php echo $contador_videos; ?>" class="modal conifer">
								    	<div class="modal-content">
											<div class="video-container">
												<iframe title="Video Destacado" width="853" height="480" src="//www.youtube.com/embed/<?php echo $video['codigo']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
										    </div>
										</div>
									</div>
						  		</div>
						  		<?php } ?>
					  		</div>
				  		</div>
			  		</div>
	  			</div>
	  		</div>
	  		<div class="row dark-grey no-margin-row" id="home_musica">
		  		<div class="col s12">
			  		<div class="row">
				  		<div class="col s12 m12 l10 offset-l1">
					  		<span class="conduit-black font36 block conifer-text uppercase centered">/Música</span>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
  		</div>
  		<?php $musica_programas = get_field("home_musica_programas", "option"); $contador_programas = 0; ?>
  		<div class="container-fluid no-margin-row" id="seccion-musica">
	  		<div class="container">
		  		<div class="row">
			  		<?php foreach ($musica_programas as $programa) { $contador_programas++; ?>
			  		<a href="<?php echo $programa['url']; ?>">
				  		<div class="col s12 m4 l4 home_musica_programa padding-1rem" rel="<?php echo $contador_programas; ?>">
					  		<center>
					  			<img alt="Portada Musica" longdesc="<?php echo $programa['portada']; ?>" src="<?php echo $programa['portada']; ?>" class="responsive-img" />
					  			<input type="hidden" id="portada-mp-<?php echo $contador_programas; ?>" name="portada-mp-<?php echo $contador_programas; ?>" value="<?php echo $programa['portada']; ?>" />
							  	<input type="hidden" id="portada-mp-hover-<?php echo $contador_programas; ?>" name="portada-mp-hover-<?php echo $contador_programas; ?>" value="<?php echo $programa['hover']; ?>" />
					  		</center>
				  		</div>
				  		<span class="no-text-link">Sección <?php echo $programa['url']; ?></span>
			  		</a>
			  		<?php } ?>
		  		</div>
	  		</div>
  		</div>
  		<?php $programas_lista = get_field("home_programas_lista", "option"); $contador_programas = 0; ?>
  		<div class="container">
	  		<div class="row dark-grey no-margin-row" id="home_programas">
		  		<div class="col s12">
			  		<div class="space20"></div>
			  		<div class="row">
				  		<div class="col s12 m12 l10 offset-l1">
					  		<span class="conduit-black font36 block conifer-text uppercase centered">/Programas</span>
					  		<div class="space20"></div>
					  		<div class="row">
						  		<?php foreach ($programas_lista as $programa) { $contador_programas++; ?>
						  		<a href="<?php echo $programa['url']; ?>">
							  		<span class="no-text-link">Programa <?php echo $programa['url']; ?></span>
							  		<div class="col s12 m6 l6 home_programa_lista" rel="<?php echo $contador_programas; ?>">
								  		<center>
								  			<img alt="Portada Lista" longdesc="<?php echo $programa['portada']; ?>" src="<?php echo $programa['portada']; ?>" class="responsive-img" />
								  			<input type="hidden" id="portada-pl-<?php echo $contador_programas; ?>" name="portada-pl-<?php echo $contador_programas; ?>" value="<?php echo $programa['portada']; ?>" />
								  			<input type="hidden" id="portada-pl-hover-<?php echo $contador_programas; ?>" name="portada-pl-hover-<?php echo $contador_programas; ?>" value="<?php echo $programa['hover']; ?>" />
								  		</center>
							  		</div>
						  		</a>
						  		<?php } ?>
					  		</div>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
	  		<div class="row dark-grey no-margin-row" id="home_host">
		  		<div class="col s12">
			  		<div class="space20"></div>
			  		<span class="conduit-black font36 block conifer-text uppercase centered">/Conductor</span>
			  		<div class="row padding-1rem">
				  		<?php $host_imagenes = get_field("home_host_imagenes", "option"); ?>
				  		<div class="col s12 m4 l4 seasons">
					  		<div class="slider">
							    <ul class="slides">
							  		<?php foreach ($host_imagenes as $host_imagen) { ?>
							  		<li>
								        <img alt="Post" longdesc="<?php echo $host_imagen['url']; ?>" src="<?php echo $host_imagen['url']; ?>" class="responsive-img">
								    </li>
							  		<?php } ?>
							    </ul>
					  		</div>
				  		</div>
				  		<div class="col s12 m8 l8 violet">
					  		<p class="conduit font20 block white-text"><?php the_field("home_host_descripcion", "option"); ?></p>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
	  		<!--
	  		<div class="row dark-grey no-margin-row" id="home_instagram">
		  		<div class="col s12">
			  		<div class="space20"></div>
			  		<div class="row">
				  		<div class="col s12 m12 l10 offset-l1">
					  		<span class="conduit-black font36 block conifer-text uppercase centered">/Instagram</span>
				  		</div>
			  		</div>
			  		<div class="row">
				  		<div class="col s12 m12 l12" id="grid_instagram">
					  		<?php echo do_shortcode('[instagram-feed]'); ?>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
	  		!-->
  		</div>

<?php get_footer(); ?>