
		<?php $lista_banners = get_field("lista_banners", "option"); $contador_banner = 0; ?>
		<?php $fecha_transmision = get_field("fecha_transmision", "option"); $transmision = false; ?>
		<?php date_default_timezone_set(get_option('timezone_string')); ?>
		<?php 
			//Calculas las Fechas
			foreach ($fecha_transmision as $fecha)
			{
				if ($fecha['dia'] == date("w")) 
				{
					$date_start = date('Y-m-d '.$fecha['start'].':00');
					$date_end = date('Y-m-d '.$fecha['end'].':00');
					$start = strtotime($date_start);
					$end = strtotime($date_end);
					
					if(time() >= $start && time() <= $end)
					{
						$transmision = true;
					} 
				}
			}
		?>
		<input type="hidden" id="start" value="<?php echo $date_start; ?>" />
		<input type="hidden" id="end" value="<?php echo $date_end; ?>" />
		<div class="slider-home">
			<?php if ($transmision) { $imagen_transmision = get_field("imagen_transmision", "option"); ?>
			<div class="video-container">
				<?php the_field("iframe_transmision", "option"); ?>
			</div>
			<?php } else { ?>
			<div class="slider">
			    <ul class="slides">
			<?php foreach ($lista_banners as $banner) { $contador_banner++; ?>
			<li>
				<img alt="Imagen Slider" longdesc="<?php echo $banner['imagen'][0]['desktop']; ?>" src="<?php echo $banner['imagen'][0]['desktop']; ?>" class="responsive-img hide-on-small-only hide-on-med-only" />
				<img alt="Imagen Slider" longdesc="<?php echo $banner['imagen'][0]['tablet']; ?>" src="<?php echo $banner['imagen'][0]['tablet']; ?>" class="responsive-img hide-on-small-only hide-on-large-only" />
				<img alt="Imagen Slider" longdesc="<?php echo $banner['imagen'][0]['mobile']; ?>" src="<?php echo $banner['imagen'][0]['mobile']; ?>" class="responsive-img hide-on-large-only hide-on-med-only" />
				<div class="caption-<?php echo $banner['orientacion_texto']; ?>">
					<span class="conduit-black font36 block" style="color: <?php echo $banner['color_texto']; ?> !important;"><?php echo $banner['titulo']; ?></span>
					<span class="conduit-black font26 block" style="color: <?php echo $banner['color_texto']; ?> !important;"><?php echo $banner['descripcion']; ?></span>
					<span class="conduit italic font26 block" style="color: <?php echo $banner['color_texto']; ?> !important;"><?php echo $banner['hashtag']; ?></span>
					<?php 
						switch ($banner['click']) { 
							case 'link': 
								echo '<a href="' . $banner['link'] . '" class="waves-effect waves-light btn conduit" style="background-color: ' . $banner['color_boton'] . ' !important; color: ' . $banner['color_texto_boton'] . ' !important;">Ver Contenido</a>'; 
								break;
							case 'youtube':
								echo '<a href="#modal_banner_' . $contador_banner . '" class="waves-effect waves-light btn conduit" style="background-color: ' . $banner['color_boton'] . ' !important; color: ' . $banner['color_texto_boton'] . ' !important;">Ver Video</a>'; 
								break;
							case 'ninguno': 
								break;
						}
					?>
				</div>
			</li>
			<?php } ?>
				</ul>
			</div>
			<?php } ?>
		</div>
		<?php $contador_banner = 0; ?>
		<?php foreach ($lista_banners as $banner) { $contador_banner++; ?>
			<?php if ($banner['click'] == 'youtube') { ?>
				<div id="modal_banner_<?php echo $contador_banner; ?>" class="modal conifer">
					<div class="modal-content">
						<div class="video-container">
							<iframe width="853" height="480" src="//www.youtube.com/embed/<?php echo $banner['codigo']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
		<div id="modal_transmision" class="modal conifer">
			<div class="modal-content">
				<div class="video-container">
					<?php the_field("iframe_transmision", "option"); ?>
				</div>
			</div>
		</div>