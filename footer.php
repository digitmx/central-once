
		<div class="container-fluid no-margin-row grey" id="footer-social">
	  		<div class="container">
		  		<div class="row">
			  		<div class="col s12 m12 l12">
				  		<div class="space20"></div>
				  		<span class="block centered" id="social-icons">
				  			<?php if (get_field("youtube", "option")) { ?>
				  			<a class="rangoon-green-text" href="<?php the_field("youtube", "option"); ?>" target="_blank">
					  			<span class="no-text-link"><?php the_field("youtube", "option"); ?></span>
				  				<i class="fa fa-youtube-square fa-3x" aria-hidden="true"></i>
				  			</a>
				  			<?php } ?>
				  			<?php if (get_field("twitter", "option")) { ?>
				  			<a class="rangoon-green-text" href="<?php the_field("twitter", "option"); ?>" target="_blank">
					  			<span class="no-text-link"><?php the_field("twitter", "option"); ?></span>
				  				<i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i>
				  			</a>
				  			<?php } ?>
				  			<?php if (get_field("facebook", "option")) { ?>
				  			<a class="rangoon-green-text" href="<?php the_field("facebook", "option"); ?>" target="_blank">
					  			<span class="no-text-link"><?php the_field("facebook", "option"); ?></span>
				  				<i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i>
				  			</a>
				  			<?php } ?>
				  			<?php if (get_field("instagram", "option")) { ?>
				  			<a class="rangoon-green-text" href="<?php the_field("instagram", "option"); ?>" target="_blank">
					  			<span class="no-text-link"><?php the_field("instagram", "option"); ?></span>
				  				<i class="fa fa-instagram fa-3x" aria-hidden="true"></i>
				  			</a>
				  			<?php } ?>
				  		</span>
				  		<?php if (get_field("hashtag", "option")) { ?>
				  		<span class="conduit-black font36 rangoon-green-text block centered"><?php the_field("hashtag", "option"); ?></span>
				  		<?php } ?>
			  		</div>
		  		</div>
	  		</div>
  		</div>
  		<div class="container-fluid no-margin-row black-rose" id="footer-canal-once">
	  		<div class="container">
		  		<div class="row">
			  		<div class="col s12 m6 l6">
				  		<?php $copyright = get_field("copyright", "option"); $contador_copyright = 0; ?>
				  		<div class="space40"></div>
				  		<?php foreach ($copyright as $row) { $contador_copyright++; ?>
					  		<span class="conduit block"><?php echo $row['texto']; ?></span>
					  		<?php if ($contador_copyright < count($copyright)) { ?>
					  		<div class="space10"></div>
					  		<?php } ?>
					  	<?php } ?>
				  		<div class="space40"></div>
			  		</div>
			  		<div class="col s12 m6 l6">
				  		<div class="space40"></div>
				  		<div class="right-align">
					  		<a href="http://oncetv-ipn.net" target="_blank">
							  	<span class="no-text-link">http://oncetv-ipn.net</span>
						  		<?php get_template_part("includes/canalonce", "svg"); ?>
					  		</a>
				  		</div>
			  		</div>
		  		</div>
	  		</div>
  		</div>
  		
		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		
		<?php wp_footer(); ?>
		
		<?php if (get_field('footer','option')) { the_field('footer','option'); } ?>

	</body>
</html>