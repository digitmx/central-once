
	$( document ).ready(function() {
		
		//Modal Setup
		$('.modal').modal({
			dismissible: true, // Modal can be dismissed by clicking outside of the modal
			opacity: .5, // Opacity of modal background
			in_duration: 300, // Transition in duration
			out_duration: 200, // Transition out duration
			starting_top: '4%', // Starting top style attribute
			ending_top: '10%', // Ending top style attribute
			ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
				
      		},
	  		complete: function() { $(this).find('iframe').attr('src', $(this).find('iframe').attr('src')); } // Callback for Modal close
		});
		
		//Slider Home
		$('.slider-home').slider({
			indicators: true,
			height: 720
		});
		
  		//Home Host Slider
		$('.home_host_slider').slick({
			autoplay: true,
			arrows: false,
			centerMode: true,
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear'
  		});
  		
  		//Section Gallery Slider
  		$('.header_section').slick({
			autoplay: true,
			arrows: false,
			centerMode: true,
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear'
  		});
  		
  		//Seasons
  		$('.seasons').slider();
		
		//Series Cover Slider
		$('.series-cover').slick({
			autoplay: true,
			arrows: false,
			centerMode: true,
			dots: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear'
  		});
  		
  		// sideNav setup
  		$('.menu_co').sideNav({
  			menuWidth: 300, // Default is 240
  			edge: 'right', // Choose the horizontal origin
  			closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
  			draggable: true // Choose whether you can drag to open on touch screens
    	});
    	
    	// Hide sideNav
    	$('#close-side-nav').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	$('.menu_co').sideNav('hide');
	    	
	    	return false;
    	});
    	
    	// home_video_destacado hover
    	$('.home_video_destacado').hover(function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_video_destacado #portada-vd-hover-'+rel).val();
	    	$(this).find('img').attr('src', image);
    	}, function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_video_destacado #portada-vd-'+rel).val();
	    	$(this).find('img').attr('src', image);
	    });
	    
	    // home_video_destacado click
	    $('.home_video_destacado').on('click', function(e) {
		    e.preventDefault();
		    var id = $(this).attr('rel');
		    $('#video_destacado_' + id).modal('open');
		    return false;
	    });
	    
	    // home_musica_programa hover
    	$('.home_musica_programa').hover(function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_musica_programa #portada-mp-hover-'+rel).val();
	    	$(this).find('img').attr('src', image);
    	}, function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_musica_programa #portada-mp-'+rel).val();
	    	$(this).find('img').attr('src', image);
	    });
	    
	    // home_programa_lista hover
    	$('.home_programa_lista').hover(function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_programa_lista #portada-pl-hover-'+rel).val();
	    	$(this).find('img').attr('src', image);
    	}, function() {
	    	var rel = $(this).attr('rel');
	    	var image = $('.home_programa_lista #portada-pl-'+rel).val();
	    	$(this).find('img').attr('src', image);
	    });
	    
	    // container_section item .link hover
	    $('.container_section .item .link').hover(function() {
		    var link = $(this);
		    var rel = $(this).attr('rel');
		    var normal = link.find('#color_base_' + rel).val();
		    var hover = link.find('#color_hover_' + rel).val();
		    link.find('.left').css('text-decoration','underline');
		    link.find('.right svg').css('fill',hover);
	    }, function() {
		    var link = $(this);
		    var rel = $(this).attr('rel');
		    var normal = link.find('#color_base_' + rel).val();
		    var hover = link.find('#color_hover_' + rel).val();
		    link.find('.left').css('text-decoration','none');
		    link.find('.right svg').css('fill',normal);
	    })

		//Slick Fix Accesibility	    
	    $('.slide-title').each(function () {
		    var $slide = $(this).parent();    
		    if ($slide.attr('aria-describedby') != undefined) { // ignore extra/cloned slides
		        $(this).attr('id', $slide.attr('aria-describedby'));
		    }
		});
	    	    
	});