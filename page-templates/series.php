<?php /* Template Name: SERIES */ ?>

<?php get_header(); ?>

		<?php get_template_part("includes/navbar", "menu"); ?>
		
		<?php $programas = get_field("programas", $post->ID); $contador_programa = 0; ?>
		<div class="space100"></div>
		<div class="container">
			<h1 class="no-text-link">Series</h1>
			<?php foreach ($programas as $programa) { $contador_programa++; ?>
			<div class="row">
				<div class="col s12 m4 l4 seasons">
					<div class="slider">
					    <ul class="slides">
						<?php foreach ($programa['galeria'] as $imagen) { ?>
				  			<li>
						        <img alt="Programa" longdesc="<?php echo $imagen['url']; ?>" src="<?php echo $imagen['url']; ?>" class="responsive-img">
						    </li>
			  			<?php } ?>
			  			</ul>
					</div>
				</div>
				<div class="col s12 m8 l8 series-container" style="background-color: <?php echo $programa['background_color']; ?> !important; color: <?php echo $programa['text_color']; ?> !important; ">
					<span class="conduit-black font36 block"><?php echo $programa['nombre']; ?></span>
					<span class="conduit font26 block italic"><?php echo $programa['horario']; ?></span>
					<p class="conduit font20"><?php echo $programa['sinopsis']; ?></p>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="space100"></div>
		
<?php get_footer(); ?>