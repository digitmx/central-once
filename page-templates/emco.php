<?php /* Template Name: EMCO */ ?>

<?php get_header(); ?>

		<?php get_template_part("includes/navbar", "menu"); ?>
		
		<div class="container" id="emco_header">
			<div class="row">
				<div class="col s12">
					<?php if (get_field("logo",$post->ID)) { ?>
					<img alt="Logo" longdesc="<?php the_field("logo", $post->ID); ?>" src="<?php the_field("logo", $post->ID); ?>" class="responsive-img" />
					<?php } ?>
					<h1 class="conduit-black font36 block white-text uppercase"><?php the_field("titulo", $post->ID); ?></h1>
				  	<h2 class="conduit font26 block white-text"><?php the_field("descripcion", $post->ID); ?></h2>
				</div>
			</div>
		</div>
		<?php $temporadas = get_field("temporadas", $post->ID); $contador_temporada = 0; ?>
		<?php $background_temporadas = get_field("background_temporadas",$post->ID); ?>
		<div class="container section"<?php if ($background_temporadas) { echo ' style="background-image:url('.$background_temporadas.');"'; }?>>
			<div class="row">	
				<div class="col s10 offset-s1">
					<?php foreach ($temporadas as $temporada) { $contador_temporada++; $contador_capitulo = 0; ?>
					<div class="space40"></div>
					<div class="row">			
						<div class="col s12 m4 l4 seasons">
							<div class="slider">
							    <ul class="slides">
								<?php foreach ($temporada['galeria'] as $imagen) { ?>
						  			<li>
								        <img alt="Temporadas" longdesc="<?php echo $imagen['url']; ?>" src="<?php echo $imagen['url']; ?>" class="responsive-img">
								    </li>
					  			<?php } ?>
					  			</ul>
							</div>
						</div>
						<div class="col s12 m8 l8 container_section" style="background-color: <?php echo $temporada['background_color']; ?>;">
							<div class="padding-1rem">
								<?php if ($temporada['nombre']) { ?>
								<span class="conduit-black font26 block uppercase" style="color: <?php echo $temporada['title_color']; ?>;"><?php echo $temporada['nombre']; ?></span>
								<?php } ?>
								<?php if ($temporada['anio']) { ?>
								<span class="conduit-black font26 block uppercase" style="color: <?php echo $temporada['title_color']; ?>;"><?php echo $temporada['anio']; ?></span>
								<?php } ?>
								<span class="conduit-black font26 block uppercase" style="color: <?php echo $temporada['title_color']; ?>;"><?php echo count($temporada['capitulos']); ?> capítulos</span>
								<?php $play_color = $temporada['title_color']; ?>
								<?php foreach ($temporada['capitulos'] as $capitulo) { $contador_capitulo++; ?>
								<div class="item">
									<!-- Modal Structure -->
							  		<div id="video_<?php echo $post->slug; ?>_<?php echo $contador_temporada.'_'.$contador_capitulo; ?>" class="modal" style="background-color: <?php echo $temporada['background_color']; ?> !important;">
								    	<div class="modal-content">
											<div class="video-container">
										    	<iframe title="Video Capítulo" width="853" height="480" src="//www.youtube.com/embed/<?php echo $capitulo['codigo']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
										    </div>
										</div>
									</div>
									<?php $unique = sanitize_title($capitulo['nombre']); ?>
									<a class="link" rel="<?php echo $unique; ?>" href="#video_<?php echo $post->slug; ?>_<?php echo $contador_temporada.'_'.$contador_capitulo; ?>">
										<div class="left conduit font26" style="color: <?php echo $temporada['episode_color']; ?>;"><?php echo $capitulo['nombre']; ?></div>
										<div class="right">
											<?php include( locate_template( 'includes/play-svg.php' ) ); ?>
										</div>
										<div class="clear"></div>
										<input type="hidden" id="color_base_<?php echo $unique; ?>" name="color_base_<?php echo $unique; ?>" value="<?php echo $temporada['title_color']; ?>" />
										<input type="hidden" id="color_hover_<?php echo $unique; ?>" name="color_hover_<?php echo $unique; ?>" value="<?php echo $temporada['episode_color']; ?>" />
									</a>
								</div>	
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		
<?php get_footer(); ?>