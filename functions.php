<?php

	//Remove Enqueue Scripts
	remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

	if( function_exists('acf_add_options_page') ) {

		acf_add_options_page();

	}

	// Thumbnails Support
	if ( function_exists( 'add_theme_support' ) ) {
	  add_theme_support( 'post-thumbnails' );
	}

	//CHANGE POST MENU LABELS
	function change_post_menu_label() {
	    global $menu;
	    global $submenu;
	    $menu[70][0] = 'Administradores';
	    echo '';
	}
    add_action( 'admin_menu', 'change_post_menu_label' );

	//Change Footer Text
	add_filter( 'admin_footer_text', 'my_footer_text' );
	add_filter( 'update_footer', 'my_footer_version', 11 );
	function my_footer_text() {
	    return '<i>Central Once</i>';
	}
	function my_footer_version() {
	    return 'Version 1.0';
	}

	// Páginas de Configuración
	add_filter('acf/options_page/settings', 'my_options_page_settings');

	function my_options_page_settings ( $options )
	{
		$options['title'] = __('Configuración');
		$options['pages'] = array(
			__('Banners'),
			__('Portada'),
			__('Custom'),
			__('Colores'),
			__('Footer')
		);

		return $options;
	}
	
	//Add Custom CSS
	add_action('wp_head','hook_css');	
	function hook_css() {
		if (get_field('custom_css','option')) 
		{
			$output="<style>".utf8_encode(get_field('custom_css','option'))."</style>";
		}
		utf8_encode(get_field('header','option'));
		echo $output;
	}
	
	//Add Custom JS
	add_action('wp_footer','hook_javascript');
	function hook_javascript() {
	
		if (get_field('custom_js','option')) 
		{
			$output="<script>".utf8_encode(get_field('custom_js','option'))."</script>";
		}
		echo utf8_encode(get_field('footer','option'));
		echo $output;
	}

	/* Definición de Directorios */
	define( 'JSPATH', get_template_directory_uri() . '/js/' );
	define( 'CSSPATH', get_template_directory_uri() . '/css/' );
	define( 'THEMEPATH', get_template_directory_uri() . '/' );
	define( 'IMGPATH', get_template_directory_uri() . '/img/' );
	define( 'SITEURL', site_url('/') );

	/* Enqueue scripts and styles. */
	function scripts() {
		// Load Materialize
		wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
		wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), '1.0.9' );
		// Load jQuery
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', JSPATH . 'app.js', array(), '1.0.26', false );
	}
	add_action( 'wp_enqueue_scripts', 'scripts' );

	//CUSTOM POST TYPES
	add_action( 'init', 'codex_custom_init' );
	function codex_custom_init() {

		/*
		//Usuarios
		$labels = array(
		    'name' => _x('Usuarios', 'post type general name'),
		    'singular_name' => _x('Usuario', 'post type singular name'),
		    'add_new' => _x('Agregar Nuevo', 'usuario'),
		    'add_new_item' => __('Agregar un Usuario'),
		    'edit_item' => __('Editar Usuario'),
		    'new_item' => __('Nuevo Usuario'),
		    'all_items' => __('Todos los Usuarios'),
		    'view_item' => __('Ver Usuario'),
		    'search_items' => __('Buscar Usuario'),
		    'not_found' =>  __('Usuarios no encontrados'),
		    'not_found_in_trash' => __('No hay Usuarios en la Papelera'),
		    'parent_item_colon' => '',
		    'menu_name' => 'Usuarios'
		);
		$args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'has_archive' => true,
		    'hierarchical' => false,
		    'menu_position' => 4,
		    'menu_icon' => 'dashicons-admin-users',
		    'supports' => array( 'title', 'custom-fields' )
		);
		register_post_type('usuario',$args);
		*/
	}

	//Funcion para identar JSON
	function indent($json)
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ($i=0; $i<=$strLen; $i++) {

	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);

	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }

	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}

	function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo indent($json);
	}
	
	function getMenuColor($post_slug)
	{
		//Read Color
		$color = get_field($post_slug.'_color', 'option');
		
	    return $color;
	}
	
	function getPrimaryColor($post_slug)
	{
		//Switch Primary Color
	    switch ($post_slug)
	    {
		    case 'home': $color = '#E4DE46 !important'; $boton = ''; $texto = ''; break;
		    case 'emco': $color = '#E4DE46 !important'; $boton = ''; $texto = ''; break;
		    case 'acusticos': $color = '#A981CD !important'; $boton = ''; $texto = ''; break;
		    case 'la-central': $color = '#813495 !important'; $boton = ''; $texto = ''; break;
		    case 'todo-terreno': $color = '#F26246 !important'; $boton = '#4ADEAD !important'; $texto = '#E4DE46 !important'; break;
		    case 'series': $color = '#E4DE46 !important'; $boton = ''; $texto = ''; break;
		    default: $color = '#E4DE46 !important'; $boton = ''; $texto = ''; break;
	    }
	    
	    //Array Response
	    $response = array(
		    'color' => $color,
		    'boton' => $boton,
		    'texto' => $texto,
		    'slug' => $post_slug
	    );
	    
	    return $response;
	}
	
	function getSecondaryColor($post_slug)
	{
		//Switch Primary Color
	    switch ($post_slug)
	    {
		    case 'home': $color = '#E4DE46 !important'; $boton = ''; break;
		    case 'emco': $color = '#E4DE46 !important'; $boton = ''; break;
		    case 'acusticos': $color = '#A981CD !important'; $boton = ''; break;
		    case 'la-central': $color = '#813495 !important'; $boton = ''; break;
		    case 'todo-terreno': $color = '#4ADEAD !important'; $boton = ''; break;
		    case 'series': $color = '#E4DE46 !important'; $boton = ''; break;
		    default: $color = '#E4DE46 !important'; $boton = ''; break;
	    }
	    
	    //Array Response
	    $response = array(
		    'color' => $color,
		    'boton' => $boton
	    );
	    
	    return $response;
	}

	//SUBIR IMAGEN A CAMPO IMG DE ACF
	function my_update_attachment($f,$pid,$t='',$c='') {
	  	wp_update_attachment_metadata( $pid, $f );
	  	if( !empty( $_FILES[$f]['name'] )) { //New upload
	    	require_once( ABSPATH . 'wp-admin/includes/file.php' );
			include( ABSPATH . 'wp-admin/includes/image.php' );
			// $override['action'] = 'editpost';
			$override['test_form'] = false;
			$file = wp_handle_upload( $_FILES[$f], $override );

			if ( isset( $file['error'] )) {
				return new WP_Error( 'upload_error', $file['error'] );
	    	}

			$file_type = wp_check_filetype($_FILES[$f]['name'], array(
				'jpg|jpeg' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
			));

			if ($file_type['type']) {
				$name_parts = pathinfo( $file['file'] );
				$name = $file['filename'];
				$type = $file['type'];
				$title = $t ? $t : $name;
				$content = $c;

				$attachment = array(
					'post_title' => $title,
					'post_type' => 'attachment',
					'post_content' => $content,
					'post_parent' => $pid,
					'post_mime_type' => $type,
					'guid' => $file['url'],
				);

				foreach( get_intermediate_image_sizes() as $s ) {
					$sizes[$s] = array( 'width' => '', 'height' => '', 'crop' => true );
					$sizes[$s]['width'] = get_option( "{$s}_size_w" ); // For default sizes set in options
					$sizes[$s]['height'] = get_option( "{$s}_size_h" ); // For default sizes set in options
					$sizes[$s]['crop'] = get_option( "{$s}_crop" ); // For default sizes set in options
	      		}

		  		$sizes = apply_filters( 'intermediate_image_sizes_advanced', $sizes );

		  		foreach( $sizes as $size => $size_data ) {
		  			$resized = image_make_intermediate_size( $file['file'], $size_data['width'], $size_data['height'], $size_data['crop'] );
		  			if ( $resized )
		  				$metadata['sizes'][$size] = $resized;
	      		}

		  		$attach_id = wp_insert_attachment( $attachment, $file['file'] /*, $pid - for post_thumbnails*/);

		  		if ( !is_wp_error( $attach_id )) {
		  			$attach_meta = wp_generate_attachment_metadata( $attach_id, $file['file'] );
		  			wp_update_attachment_metadata( $attach_id, $attach_meta );
	      		}

		  		return array(
		  			'pid' =>$pid,
		  			'url' =>$file['url'],
		  			'file'=>$file,
		  			'attach_id'=>$attach_id
		  		);
	    	}
	  	}
	}

?>
