<?php get_header(); ?>

		<?php get_template_part("includes/navbar", "menu"); ?>
		
		<?php
			if ( have_posts() ) {
		    	while ( have_posts() ) 
		    	{
					the_post(); ?>
		<div class="space20"></div>
		<div class="container" id="page_header">
			<div class="row">
				<div class="col s12">
					<?php if ( has_post_thumbnail() ) { ?>
					<img src="<?php the_post_thumbnail_url(); ?>" class="responsive-img" />
					<?php } ?>
					<h1 class="conduit-black font36 block white-text uppercase"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		<div class="container"><?php the_content(); ?></div>
		<div class="space20"></div>
		<?php
				}
			}
		?>
		
<?php get_footer(); ?>